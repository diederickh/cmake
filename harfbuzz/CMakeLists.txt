# Based on https://gist.github.com/roxlu/2dcc30c4f34e7737d506
#
# You can provide the following options:
#
# FREETYPE_INC_DIR             Path to the ftbuild.h file.
# FREETYPE_LIB_DIR             Path to the dir where you can find freetype.lib
#
cmake_minimum_required(VERSION 2.8)
project(harfbuzz)

message(STATUS "FREETYPE_INC_DIR: ${FREETYPE_INC_DIR}")
message(STATUS "FREETYPE_LIB_DIR: ${FREETYPE_LIB_DIR}")

set(bd ${CMAKE_CURRENT_LIST_DIR})
set(sd ${bd}/src)
set(id ${bd}/src)

find_library(FREETYPE_LIB freetype PATHS ${FREETYPE_LIB_DIR})
find_path(FREETYPE_INC ft2build.h PATHS ${FREETYPE_INC_DIR} ${FREETYPE_INC_DIR}/freetype2)

# Defines (tested on Win only)
add_definitions(
  -DHAVE_OT
  -DHAVE_ATEXIT
  -DHAVE_NO_MT
  -DHB_DISABLE_DEPRECATED
  -DHAVE_GETPAGESIZE
  -DHAVE_INTTYPES_H
  -DHAVE_ISATY
  -DHAVE_MEMORY_H
  -DHAVE_STDINT_H
  -DHAVE_STDLIB_H
  -DHAVE_STRING_H
  -DHAVE_SYS_STATE_H
  -DHAVE_SYS_TYPES_H
  -DHAVE_WINDOWS_H
  -DSTDC_HEADERS
  )


# Headers
set(harfbuzz_header_files
  ${sd}/hb.h
  ${sd}/hb-blob.h
  ${sd}/hb-buffer.h
  ${sd}/hb-common.h
  ${sd}/hb-deprecated.h
  ${sd}/hb-face.h
  ${sd}/hb-font.h
  ${sd}/hb-ot.h
  ${sd}/hb-ot-layout.h
  ${sd}/hb-ot-tag.h
  ${sd}/hb-set.h
  ${sd}/hb-shape.h
  ${sd}/hb-shape-plan.h
  ${sd}/hb-unicode.h
  ${sd}/hb-version.h
  )

# Source files
set(harfbuzz_source_files
  ${sd}/hb-blob.cc
  ${sd}/hb-buffer.cc
  ${sd}/hb-buffer-serialize.cc
  ${sd}/hb-common.cc
  ${sd}/hb-face.cc
  ${sd}/hb-font.cc
  ${sd}/hb-ot-tag.cc
  ${sd}/hb-set.cc
  ${sd}/hb-shape.cc
  ${sd}/hb-shape-plan.cc
  ${sd}/hb-shaper.cc
  ${sd}/hb-unicode.cc
  ${sd}/hb-warning.cc

  ${sd}/hb-atomic-private.hh
  ${sd}/hb-buffer-private.hh
  ${sd}/hb-cache-private.hh
  ${sd}/hb-face-private.hh
  ${sd}/hb-font-private.hh
  ${sd}/hb-mutex-private.hh
  ${sd}/hb-object-private.hh
  ${sd}/hb-open-file-private.hh
  ${sd}/hb-open-type-private.hh
  ${sd}/hb-ot-head-table.hh
  ${sd}/hb-ot-hhea-table.hh
  ${sd}/hb-ot-hmtx-table.hh
  ${sd}/hb-ot-maxp-table.hh
  ${sd}/hb-ot-name-table.hh
  ${sd}/hb-private.hh
  ${sd}/hb-set-private.hh
  ${sd}/hb-shape-plan-private.hh
  ${sd}/hb-shaper-impl-private.hh
  ${sd}/hb-shaper-list.hh
  ${sd}/hb-shaper-private.hh
  ${sd}/hb-unicode-private.hh
  ${sd}/hb-utf-private.hh

  # Open Type
  ${sd}/hb-ot-layout.cc
  ${sd}/hb-ot-map.cc
  ${sd}/hb-ot-shape.cc
  ${sd}/hb-ot-shape-complex-arabic.cc
  ${sd}/hb-ot-shape-complex-default.cc
  ${sd}/hb-ot-shape-complex-hangul.cc
  ${sd}/hb-ot-shape-complex-hebrew.cc
  ${sd}/hb-ot-shape-complex-indic.cc
  ${sd}/hb-ot-shape-complex-indic-table.cc
  ${sd}/hb-ot-shape-complex-myanmar.cc
  ${sd}/hb-ot-shape-complex-sea.cc
  ${sd}/hb-ot-shape-complex-thai.cc
  ${sd}/hb-ot-shape-complex-tibetan.cc
  ${sd}/hb-ot-shape-fallback.cc
  ${sd}/hb-ot-shape-normalize.cc

  ${sd}/hb-ot-layout-common-private.hh
  ${sd}/hb-ot-layout-gdef-table.hh
  ${sd}/hb-ot-layout-gpos-table.hh
  ${sd}/hb-ot-layout-gsubgpos-private.hh
  ${sd}/hb-ot-layout-gsub-table.hh
  ${sd}/hb-ot-layout-jstf-table.hh
  ${sd}/hb-ot-layout-private.hh
  ${sd}/hb-ot-map-private.hh
  ${sd}/hb-ot-shape-complex-arabic-fallback.hh
  ${sd}/hb-ot-shape-complex-arabic-table.hh
  ${sd}/hb-ot-shape-complex-indic-private.hh
  ${sd}/hb-ot-shape-complex-private.hh
  ${sd}/hb-ot-shape-fallback-private.hh
  ${sd}/hb-ot-shape-normalize-private.hh
  ${sd}/hb-ot-shape-private.hh

  ${sd}/hb-ot-shape.h
  )

# Compile with freetype.
if (FREETYPE_LIB)

  include_directories(${FREETYPE_INC})
  
  add_definitions(
    -DHAVE_FREETYPE=1
    -DHAVE_FT_FACE_GETCHARVARIANTINDEX=1
    )
  
  list(APPEND harfbuzz_header_files ${sd}/hb-ft.h)
  list(APPEND harfbuzz_source_files ${sd}/hb-ft.cc)
endif()

# Build in UCDN
add_definitions(-DHAVE_UCDN)
list(APPEND harfbuzz_source_files
  ${sd}/hb-ucdn.cc                                        
  ${sd}/hb-ucdn/ucdn.c
  ${sd}/hb-ucdn/unicodedata_db.h
  )
include_directories(${sd}/hb-ucdn)

# Windows, Uniscribe
add_definitions(-DHAVE_UNISCRIBE)
list(APPEND harfbuzz_source_files ${sd}/hb-uniscribe.cc)
list(APPEND harfbuzz_header_files ${sd}/hb-uniscribe.h)

# Create lib.
add_library(harfbuzz STATIC ${harfbuzz_source_files})

install(TARGETS harfbuzz ARCHIVE DESTINATION lib)
install(FILES ${harfbuzz_header_files} DESTINATION include/harfbuzz)
